<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RegisterForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('warzoneID', TextType::class, ['label'=>'WarzoneID']);
        $builder->add('email', TextType::class, ['label'=>'E-mail']);
        $builder->add('password', PasswordType::class, ['label'=>'Contraseña']);
        $builder->add('img',FileType::class, ['label'=>'Imagen']);
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
       //para hacerlo de forma automatica
        $resolver->setDefaults(['data_class'=>User::class]);
    }
    
}