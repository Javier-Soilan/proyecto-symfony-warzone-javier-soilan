<?php

namespace App\Form;

use App\Entity\Post;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PostForm extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('gameMode', TextType::class, ['label'=>'Modo de juego:']);
        $builder->add('minKD', ChoiceType::class, ['label'=>'KD min:',
            'choices' => [
                '0' => 0,
                '0,5' => 0.5,
                '1' => 1,
                '1,5' => 1.5,
                '2' => 2,
                '2,5' => 2.5,
                '3' => 3,
                '3,5' => 3.5,
                '4' => 4,
                '4+' => 100,
            ]
        ]);
        $builder->add('maxKD', ChoiceType::class, ['label'=>'KD max:',
            'choices' => [
                '0,5' => 0.5,
                '1' => 1,
                '1,5' => 1.5,
                '2' => 2,
                '2,5' => 2.5,
                '3' => 3,
                '3,5' => 3.5,
                '4' => 4,
                '4+' => 100,
            ]
        ]);
        $builder->add('language', TextType::class, ['label'=>'Idioma:']);
        $builder->add('players', ChoiceType::class, ['label'=>'Nº jugadores:',
        'choices' => [
            '1' => 1,
            '2' => 2,
            '3' => 3,
        ]
    ]);
        $builder->add('microphone', ChoiceType::class, ['label'=>'Chat de voz:',
        'choices' => [
            'Si'=> 'Si',
            'No'=> 'No',
        ]
    ]);
        $builder->add('platform', ChoiceType::class, ['label'=>'Plataforma:',
        'choices' => [
            'PC'=> 'PC',
            'Consola'=> 'Consola',
            'Indiferente'=> 'Indefente',
        ]
    ]);
        
        
    }

    public function configureOptions(OptionsResolver $resolver)
    {
       //para hacerlo de forma automatica
        $resolver->setDefaults(['data_class'=>Post::class]);
    }
    
}