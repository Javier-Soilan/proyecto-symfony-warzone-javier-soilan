<?php

namespace App\Entity;

use App\Repository\PostRepository;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PostRepository::class)
 */
class Post
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $gameMode;

    /**
     * @ORM\Column(type="float")
     */
    private $minKD;

    /**
     * @ORM\Column(type="float")
     */
    private $maxKD;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $language;

    /**
     * @ORM\Column(type="integer")
     */
    private $players;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $platform;

    /**
     * @ORM\Column(type="string")
     */
    private $microphone;

    /**
     * @ORM\Column(type="datetime")
     */
    private $publicationDate;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="posts")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;


    public function __construct()
    {
        $this->publicationDate = new DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getGameMode(): ?string
    {
        return $this->gameMode;
    }

    public function setGameMode(string $gameMode): self
    {
        $this->gameMode = $gameMode;

        return $this;
    }

    public function getMinKD(): ?float
    {
        return $this->minKD;
    }

    public function setMinKD(float $minKD): self
    {
        $this->minKD = $minKD;

        return $this;
    }

    public function getMaxKD(): ?float
    {
        return $this->maxKD;
    }

    public function setMaxKD(float $maxKD): self
    {
        $this->maxKD = $maxKD;

        return $this;
    }

    public function getLanguage(): ?string
    {
        return $this->language;
    }

    public function setLanguage(string $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getPlayers(): ?int
    {
        return $this->players;
    }

    public function setPlayers(int $players): self
    {
        $this->players = $players;

        return $this;
    }

    public function getPlatform(): ?string
    {
        return $this->platform;
    }

    public function setPlatform(string $platform): self
    {
        $this->platform = $platform;

        return $this;
    }

    public function getMicrophone(): ?string
    {
        return $this->microphone;
    }

    public function setMicrophone(string $microphone): self
    {
        $this->microphone = $microphone;

        return $this;
    }

    public function getPublicationDate(): ?\DateTimeInterface
    {
        return $this->publicationDate;
    }

    public function setPublicationDate(\DateTimeInterface $publicationDate): self
    {
        $this->publicationDate = $publicationDate;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }
}
