<?php

namespace App\Controller;

use App\Entity\Post;
use App\Entity\User;
use App\Form\RegisterForm;
use App\Form\PostForm;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class DefaultController extends AbstractController
{

    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, UserPasswordEncoderInterface $passwordEnconde, EntityManagerInterface $manager )
    {
        $form = $this->createForm(RegisterForm::class);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){
          
            $user = $form->getData();
            $user->setPassword($passwordEnconde->encodePassword($user, $user->getPassword()));
            $manager->persist($user);
            $manager->flush();

            return $this->redirectToRoute('app_login');
        }

        return $this->render(
            'Register/register.html.twig',
            ['registerForm'=> $form->createView()]
            );
        
        }

        /**
         * @Route("/home", name="home")
         */
        public function home(EntityManagerInterface $doctrine)
        {
            $repo = $doctrine->getRepository(Post::class);
            $posts = $repo->findAll();

            return $this->render("Home/home.html.twig", ['posts'=>$posts]);
        }


        /**
         * @Route("/home/createpost", name="post")
         */
        public function postForm(Request $request, UserPasswordEncoderInterface $passwordEnconde, EntityManagerInterface $manager )
        {
            $form = $this->createForm(PostForm::class);
    
            $form->handleRequest($request);
    
            if($form->isSubmitted() && $form->isValid()){
              
                $post = $form->getData();
                $post->setUser($this->getUser());
                $manager->persist($post);
                $manager->flush();
    
                return $this->redirectToRoute('home');
            }
    
            return $this->render(
                'Post/post.html.twig',
                ['postForm'=> $form->createView()]
            );
         
            
            }

        /**
         * @Route("/home/user", name="user")
         */
        public function user()
        {
            return $this->render("User/user.html.twig");
        }

        /**
         * @Route("/home/user/post-delete/{id}" , name="deletePost")
         */
        public function deletPost($id, EntityManagerInterface $doctrine)
        {
            $post = $doctrine->getRepository(Post::class)->find($id);

            $this->getUser()->removePost($post);
            $doctrine->remove($post);
            $doctrine->flush();

            return $this->redirectToRoute('user');
        }
        
}